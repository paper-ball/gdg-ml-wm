package com.paper.ball.gdg.mlwm

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.paper.ball.gdg.mlwm.mlkit.MLKitFragment
import com.paper.ball.gdg.mlwm.sync.SyncFragment
import com.paper.ball.gdg.mlwm.wm.WMFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_mlkit -> {
                loadFragment(MLKitFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_wm -> {
                loadFragment(WMFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_sync -> {
                loadFragment(SyncFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        loadFragment(MLKitFragment())
    }

    private fun loadFragment(fragment: Fragment?): Boolean {
        fragment?.let {
            supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit()
            return true
        }

        return false
    }
}
